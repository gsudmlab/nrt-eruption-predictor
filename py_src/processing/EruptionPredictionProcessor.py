"""
 * NRT-Eruption-Predictor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2023 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import asyncio
import traceback
from datetime import timedelta, datetime
from logging import Logger

import joblib
from pandas import DataFrame
from sktime.classification.compose import TimeSeriesForestClassifier

from py_src.databases.RESTFulDBAccessor import RESTFulDBAccessor
from py_src.datatypes.EruptionPrediction import EruptionPrediction
from py_src.datatypes.HARPParams import HARPParams


class EruptionPredictionProcessor:

    def __init__(self, db_accessor: RESTFulDBAccessor, logger: Logger, model_loc: str, batch_size: int):
        self._db_accessor = db_accessor
        self._model: TimeSeriesForestClassifier = joblib.load(model_loc)
        self._logger = logger
        self._batch_size = batch_size

    def run(self):
        asyncio.run(self.__run_async())

    async def __run_async(self):
        time = datetime.now()
        self._logger.info("EruptionPredictionProcessor initiated at %s", time)
        try:
            harps = self._db_accessor.get_unprocessed()
            for chunk_num in range(0, int(len(harps) / self._batch_size) + 1):
                chunk_start = chunk_num * self._batch_size
                self._logger.info("Processing batch starting at: %s", str(chunk_start))
                futures = None
                if (chunk_start + self._batch_size) < len(harps):
                    futures = [self.__run_gen_async(harps[x + chunk_start]) for x in range(0, self._batch_size)]
                else:
                    futures = [self.__run_gen_async(harps[x]) for x in range(chunk_start, len(harps))]

                await asyncio.gather(*futures)

        except Exception as e:
            self._logger.error('EruptionPredictionProcessor.__run_async Failed with: %s', str(e))
            self._logger.debug('EruptionPredictionProcessor.__run_async Traceback: %s', traceback.format_exc())

        time = datetime.now()
        self._logger.info("EruptionPredictionProcessor completed at %s", time)

    async def __run_gen_async(self, harp):
        try:
            start = harp.obs_time - timedelta(hours=12)
            obs_time = harp.obs_time
            params_obj = await self._db_accessor.get_params_for_harp_between_async(harp.harp_num, start, obs_time)
            if params_obj is not None:
                avail_times = await self._db_accessor.get_times_for_available_files_in_harp_between_async(harp.harp_num,
                                                                                                          start,
                                                                                                          obs_time)
                if avail_times is not None and (len(avail_times.index) * 0.90) < len(params_obj.params.index):
                    # Verify the data length and the times data could be available are the same
                    first_obs_time = params_obj.params.tail(1).iloc[0]['ObsStart']
                    duration = obs_time - first_obs_time
                    duration_in_s = duration.total_seconds()
                    hours = divmod(duration_in_s, 3600)[0]
                    if hours >= 12.0:
                        data = params_obj.params
                        cleaned_data = self.__clean_data(data)
                        if cleaned_data is not None:
                            erupt_results = self.__get_prediction(harp.harp_num, cleaned_data)
                            erupt_results.ObsStart = obs_time
                            await self._db_accessor.save_erupt_prediction_async(erupt_results)
                        else:
                            erupt_results = self.__get_null_prediction(harp.harp_num, params_obj)
                            erupt_results.ObsStart = obs_time
                            await self._db_accessor.save_erupt_prediction_async(erupt_results)
                    else:
                        erupt_results = self.__get_null_prediction(harp.harp_num, params_obj)
                        erupt_results.ObsStart = obs_time
                        await self._db_accessor.save_erupt_prediction_async(erupt_results)
        except Exception as e:
            self._logger.error('EruptionPredictionProcessor.__run_gen_async Failed with: %s', str(e))
            self._logger.debug('EruptionPredictionProcessor.__run_gen_async Traceback: %s', traceback.format_exc())

    @staticmethod
    def __get_null_prediction(harp_num: int, params_obj: HARPParams):
        df_tmp = params_obj.params
        erupt_result = EruptionPrediction(harp_num, df_tmp.iloc[0]['ObsStart'], None, None,
                                          df_tmp.iloc[-1]['LAT_MIN'], df_tmp.iloc[-1]['LON_MIN'],
                                          df_tmp.iloc[-1]['LAT_MAX'], df_tmp.iloc[-1]['LON_MAX'],
                                          df_tmp.iloc[-1]['CRVAL1'], df_tmp.iloc[-1]['CRVAL2'],
                                          df_tmp.iloc[-1]['CRLN_OBS'], df_tmp.iloc[-1]['CRLT_OBS'])
        return erupt_result

    def __get_prediction(self, harp_num: int, data: DataFrame):
        pred_data = []
        USFLUX = data['USFLUX']
        TOTUSJZ = data['TOTUSJZ']
        TOTUSJH = data['TOTUSJH']
        ABSNJZH = data['ABSNJZH']
        SAVNCPP = data['SAVNCPP']
        TOTPOT = data['TOTPOT']
        pred_data.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT])
        pred_data = DataFrame(pred_data, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT'])
        result = self._model.predict_proba(pred_data)

        erupt_result = EruptionPrediction(harp_num, data.iloc[0]['ObsStart'], result[0][1], result[0][0],
                                          data.iloc[-1]['LAT_MIN'], data.iloc[-1]['LON_MIN'],
                                          data.iloc[-1]['LAT_MAX'], data.iloc[-1]['LON_MAX'],
                                          data.iloc[-1]['CRVAL1'], data.iloc[-1]['CRVAL2'],
                                          data.iloc[-1]['CRLN_OBS'], data.iloc[-1]['CRLT_OBS'])
        return erupt_result

    def __clean_data(self, data: DataFrame) -> DataFrame:
        data = data.sort_index()
        if 60 > len(data.index) > 50:
            for i in range(len(data.index) - 1):
                start_time = data.iloc[i]['ObsStart']
                next_time = data.iloc[i + 1]['ObsStart']
                duration = next_time - start_time
                duration_in_s = duration.total_seconds()
                mins = divmod(duration_in_s, 60)[0]
                if mins > 13:
                    nex_obs = start_time + timedelta(minutes=12)
                    vals = [nex_obs]
                    for j in range(1, len(data.iloc[i])):
                        vals.append(None)
                    data = data.append(vals, ignore_index=False)
                    return self.__clean_data(data)
        elif len(data.index) < 50:
            return None

        if len(data.index) < 60:
            return None

        data.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        return data
